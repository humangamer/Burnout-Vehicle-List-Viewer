﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Reflection;
using System.Collections;
using System.Diagnostics;
using System.Globalization;

namespace VehicleListViewer
{
    public partial class DebugViewer : Control
    {
        private object _selectedObject;
        public object SelectedObject
        {
            get
            {
                return _selectedObject;
            }
            set
            {
                _selectedObject = value;
                UpdateDisplay();
            }
        }

        private Dictionary<string, object> Fields;
        private ListView list;

        public DebugViewer()
        {
            Fields = new Dictionary<string, object>();
            //DoubleBuffered = true;

            list = new ListView();
            list.View = View.Details;
            list.Dock = DockStyle.Fill;
            list.FullRowSelect = true;
            list.GridLines = true;
            list.MultiSelect = false;
            list.DoubleClick += List_DoubleClick;

            list.Columns.Add("Index", 30);
            list.Columns.Add("Name", 60);
            list.Columns.Add("Value", 240);

            Controls.Add(list);
        }

        private void List_DoubleClick(object sender, EventArgs e)
        {
            if (list.SelectedIndices.Count == 0)
                return;
            int index = list.SelectedIndices[0];
            object obj = Fields.Values.ToList()[index];

            ProcessObject(obj);
        }

        private void View_DoubleClick(object sender, EventArgs e)
        {
            SuperListView listView = (SuperListView)sender;
            if (listView.SelectedIndices.Count == 0)
                return;
            int index = listView.SelectedIndices[0];

            IList list = listView.List;
            if (index > list.Count)
                return;
            object obj = list[index];

            ProcessObject(obj);
        }

        private string GetPrimitiveString(object obj)
        {
            string value;
            if (obj is byte)
            {
                value = ((byte)obj).ToString("X2");
            }
            else if (obj is sbyte)
            {
                value = ((sbyte)obj).ToString("X2");
            }
            else if (obj is char)
            {
                value = obj.ToString();
            }
            else if (obj is short)
            {
                value = ((short)obj).ToString("X4");
            }
            else if (obj is ushort)
            {
                value = ((ushort)obj).ToString("X4");
            }
            else if (obj is int)
            {
                value = ((int)obj).ToString("X8");
            }
            else if (obj is uint)
            {
                value = ((uint)obj).ToString("X8");
            }
            else if (obj is long)
            {
                value = ((long)obj).ToString("X16");
            }
            else if (obj is ulong)
            {
                value = ((ulong)obj).ToString("X16");
            }
            else if (obj is float)
            {
                value = ((float)obj).ToString(".00");
            }
            else if (obj is double)
            {
                value = ((double)obj).ToString(".0000");
            }
            else
            {
                value = obj.ToString();
            }
            return value;
        }

        private void ProcessObject(object obj)
        {
            if (obj.GetType().IsPrimitive || obj is string)
            {
                string value = GetPrimitiveString(obj);

                Form editForm = new Form();
                editForm.FormBorderStyle = FormBorderStyle.FixedDialog;
                editForm.MaximizeBox = false;
                editForm.MinimizeBox = false;
                editForm.Size = new Size(287 + 16, (44 + 39) + 23 + 12);// 16 is the x border size and 39 is the y border size
                editForm.Text = "Edit Value";

                TextBox txtBox = new TextBox();
                txtBox.Location = new Point(12, 12);
                txtBox.Size = new Size(176, 20);
                txtBox.Text = value;

                SuperButton btnOk = new SuperButton(editForm, txtBox);
                btnOk.Location = new Point(176 + 24, 11);
                btnOk.Size = new Size(75, 22);
                btnOk.Text = "OK";
                btnOk.Click += BtnOk_Click;

                SuperButton btnDecrypt = new SuperButton(editForm, txtBox);
                btnDecrypt.Location = new Point(12, 31 + 12);
                btnDecrypt.Size = new Size(75, 23);
                btnDecrypt.Text = "Decrypt";
                btnDecrypt.Click += BtnDecrypt_Click;

                editForm.Controls.Add(txtBox);
                editForm.Controls.Add(btnOk);

                editForm.AcceptButton = btnOk;

                editForm.Controls.Add(btnDecrypt);

                editForm.ShowDialog(this);
                
            }
            else if (obj is IList)
            {
                IList list = (IList)obj;

                Form listForm = new Form();
                //listForm.FormBorderStyle = FormBorderStyle.FixedDialog;
                //listForm.MaximizeBox = false;
                listForm.MinimizeBox = false;

                SuperListView view = new SuperListView(list);

                view.View = View.Details;
                view.Dock = DockStyle.Fill;
                view.FullRowSelect = true;
                view.GridLines = true;
                view.MultiSelect = false;
                view.DoubleClick += View_DoubleClick;

                view.Columns.Add("Index", 30);
                view.Columns.Add("Name", 240);

                for (int i = 0; i < list.Count; i++)
                {
                    string[] listItem =
                    {
                        i.ToString(),
                        list[i].ToString()
                    };
                    view.Items.Add(new ListViewItem(listItem));
                }

                listForm.Controls.Add(view);

                listForm.ShowDialog(this);
            }
            else
            {
                DebugDlg dlg = new DebugDlg(obj);
                dlg.ShowDialog(this);
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            SuperButton button = (SuperButton)sender;

            button.Form.Close();
        }

        private void BtnDecrypt_Click(object sender, EventArgs e)
        {
            SuperButton button = (SuperButton)sender;

            ulong encrypted;
            if (!ulong.TryParse(button.TextBox.Text, NumberStyles.AllowHexSpecifier, CultureInfo.CurrentCulture, out encrypted))
            {
                MessageBox.Show(this, "Failed to Decrypt", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            EncryptedString s = new EncryptedString(encrypted);

            button.TextBox.Text = s.Value;
        }

        private void UpdateDisplay()
        {
            list.Items.Clear();
            object obj = _selectedObject;
            if (obj == null)
                return;
            Type t = obj.GetType();
            FieldInfo[] fields = t.GetFields();
            foreach (FieldInfo field in fields)
            {
                Fields.Add(field.Name, field.GetValue(obj));
            }

            PropertyInfo[] properties = t.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                try
                {
                    Fields.Add(property.Name, property.GetValue(obj));
                }
                catch (TargetParameterCountException e)
                {
                    Debug.WriteLine(e.Message);
                    Debug.WriteLine(e.StackTrace);
                }
            }

            List<string> keys = Fields.Keys.ToList();
            List<object> values = Fields.Values.ToList();
            for (int i = 0; i < Fields.Count; i++)
            {
                string value;
                if (values[i].GetType().IsPrimitive || values[i] is string)
                    value = GetPrimitiveString(values[i]);
                else
                    value = values[i].ToString();
                string[] listItem = 
                {
                    i.ToString(),
                    keys[i],
                    value
                };
                list.Items.Add(new ListViewItem(listItem));
            }
        }

        private class SuperListView : ListView
        {
            public IList List
            {
                get;
                private set;
            }

            public SuperListView(IList list) : base()
            {
                List = list;
            }
        }

        private class SuperButton : Button
        {
            public Form Form
            {
                get;
                private set;
            }

            public TextBox TextBox
            {
                get;
                private set;
            }

            public SuperButton(Form form, TextBox textBox) : base()
            {
                Form = form;
                TextBox = textBox;
            }
        }
    }
}
