﻿using BundleFormat;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VehicleListViewer
{
    public partial class MainForm : Form
    {
        private VehicleList currentList;
        private BND2Archive currentArchive;
        private int entryIndex;

        public MainForm()
        {
            InitializeComponent();
        }

        private void UpdateDisplay()
        {
            lstVehicles.Items.Clear();

            if (currentList == null)
                return;

            for (int i = 0; i < currentList.Entries.Count; i++)
            {
                Vehicle vehicle = currentList.Entries[i];

                string[] value = {
                    vehicle.Index.ToString("D3"),
                    //vehicle.ID.Encrypted.ToString("X16"),
                    vehicle.ID.Value,
                    vehicle.CarBrand,
                    vehicle.CarName,
                    vehicle.WheelType,
                    vehicle.Category.ToString(),
                    //Convert.ToString(vehicle.Category, 2).PadLeft(6, '0'),
                    Convert.ToString(vehicle.Flags, 2).PadLeft(18, '0'),
                    vehicle.BoostType.ToString(),
                    vehicle.MaxSpeedNoBoost.ToString("X2"),
                    vehicle.MaxSpeedBoost.ToString("D3"),
                    vehicle.NewUnknown.ToString(),
                    vehicle.FinishType.ToString(),
                    vehicle.Color.ToString(),
                    vehicle.DisplaySpeed.ToString(),
                    vehicle.DisplayBoost.ToString(),
                    vehicle.DisplayStrength.ToString(),
                    vehicle.EngineID1.Value,
                    Util.GetEngineFilenameByID(vehicle.EngineID1.Value),
                    vehicle.GroupID.ToString(),
                    vehicle.EngineID2.Value,
                    Util.GetEngineFilenameByID(vehicle.EngineID2.Value),
                    vehicle.GroupIDAlt.ToString()
                    //vehicle.Unknown15.ToString(),
                    //vehicle.Unknown20.ToString(),
                    //vehicle.Unknown27.ToString()
                };
                lstVehicles.Items.Add(new ListViewItem(value));
            }

            lstVehicles.ListViewItemSorter = new VehicleSorter(0);
            lstVehicles.Sort();
        }

        public void Open(string path, bool console = false)
        {
            Stream s = File.Open(path, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(s);
            byte[] b = br.ReadBytes(4);
            br.BaseStream.Seek(0, SeekOrigin.Begin);
            string id = Encoding.ASCII.GetString(b);
            if (id == "bnd2")
            {
                BND2Archive archive = br.ReadBND2Archive(console);
                if (archive.Entries.Count == 0)
                {
                    br.Close();
                    MessageBox.Show(this, "Invalid VehicleList Bundle", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                currentArchive = archive;

                // It might not always be index 0
                // TODO: Figure out how to determine the index
                int index = 0;
                entryIndex = index;

                BND2Entry entry = archive.Entries[index];
                byte[] data = entry.Data;
                MemoryStream ms = new MemoryStream(data);
                BinaryReader mbr = new BinaryReader(ms);
                currentList = mbr.ReadVehicleList(console);
                mbr.Close();
            }
            else
            {
                currentArchive = null;
                currentList = br.ReadVehicleList(console);
            }
            br.Close();

            UpdateDisplay();
        }
        
        private void Open(bool console)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Supported Files|*.BIN;*.BUNDLE|Binary Files|*.BIN|Bundle Files|*.BUNDLE";
            DialogResult result = ofd.ShowDialog(this);
            if (result != DialogResult.Cancel)
            {
                if (ofd.FileName == null || ofd.FileName.Length <= 0 || !File.Exists(ofd.FileName))
                    return;

                Open(ofd.FileName, console);
            }
        }
        
        private void Save(bool console)
        {
            SaveAs(console);
        }

        private void SaveAs(bool console)
        {
            if (currentList == null)
                return;
            SaveFileDialog sfd = new SaveFileDialog();
            if (currentArchive == null)
                sfd.Filter = "Binary Files|*.BIN";
            else
                sfd.Filter = "Bundle Files|*.BUNDLE";
            DialogResult result = sfd.ShowDialog(this);
            if (result != DialogResult.Cancel)
            {
                if (sfd.FileName == null || sfd.FileName.Length <= 0)
                    return;

                Stream s = sfd.OpenFile();
                BinaryWriter bw = new BinaryWriter(s);
                if (currentArchive == null)
                {
                    bw.WriteVehicleList(currentList, console);
                }
                else
                {
                    MemoryStream ms = new MemoryStream();
                    BinaryWriter mbw = new BinaryWriter(ms);
                    mbw.WriteVehicleList(currentList, console);
                    byte[] data = ms.ToArray();
                    mbw.Close();

                    currentArchive.Entries[entryIndex].Data = data;
                    currentArchive.Entries[entryIndex].Dirty = true;
                    //bw.WriteBND2Archive(currentArchive, console);
                    bw.WriteBND2Archive(currentArchive);
                }
                bw.Flush();
                bw.Close();
            }
        }

        private void EditSelectedEntry()
        {
            if (lstVehicles.SelectedItems.Count > 1)
                return;
            if (currentList == null || lstVehicles.SelectedIndices.Count <= 0)
                return;

            int index;
            if (!int.TryParse(lstVehicles.SelectedItems[0].Text, out index))
                return;
            Vehicle vehicle = currentList.Entries[index];

            VehicleEditor editor = new VehicleEditor();
            editor.Vehicle = vehicle;
            editor.OnDone += Editor_OnDone;
            editor.ShowDialog(this);
        }

        private void AddItem()
        {
            if (currentList == null)
                return;
            Vehicle vehicle = new Vehicle();
            vehicle.Index = currentList.Entries.Count;
            vehicle.ID = new EncryptedString("");

            VehicleEditor editor = new VehicleEditor();
            editor.Vehicle = vehicle;
            editor.OnDone += Editor_OnDone1; ;
            editor.ShowDialog(this);
        }

        private void Find()
        {
            MessageBox.Show(this, "Not Implemented", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Editor_OnDone1(Vehicle vehicle)
        {
            currentList.Entries.Add(vehicle);
            UpdateDisplay();
        }

        private void ShowHelp()
        {
            MessageBox.Show(this, "Not Implemented", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ShowAbout()
        {
            AboutDlg dlg = new AboutDlg();
            dlg.ShowDialog(this);
        }

        private void Editor_OnDone(Vehicle vehicle)
        {
            currentList.Entries[vehicle.Index] = vehicle;
            UpdateDisplay();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Open(true);
        }

        private void openPCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Open(false);
        }

        private void savePCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save(false);
        }

        private void lstVehicles_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            EditSelectedEntry();
        }
        
        private void lstVehicles_SelectedIndexChanged(object sender, EventArgs e)
        {
            stlStatusLabel.Text = lstVehicles.SelectedItems.Count + " Item(s) Selected";
        }

        private void lstVehicles_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            int column = e.Column;

            if (lstVehicles.ListViewItemSorter is VehicleSorter)
            {
                VehicleSorter sorter = (VehicleSorter)lstVehicles.ListViewItemSorter;
                if (sorter.column == column)
                {
                    sorter.swap();
                    lstVehicles.Sort();
                    return;
                }
            }

            lstVehicles.ListViewItemSorter = new VehicleSorter(column);
            lstVehicles.Sort();
        }

        private class VehicleSorter : IComparer
        {
            public int column;
            private bool direction;

            public VehicleSorter(int column)
            {
                this.column = column;
                this.direction = false;
            }

            public int Compare(object x, object y)
            {
                ListViewItem itemX = (ListViewItem)x;
                ListViewItem itemY = (ListViewItem)y;

                if (column > itemX.SubItems.Count || column > itemY.SubItems.Count)
                {
                    if (this.direction)
                        return -1;
                    return 1;
                }

                string iX = itemX.SubItems[column].Text;
                string iY = itemY.SubItems[column].Text;

                int iXint;
                int iYint;

                if (int.TryParse(iX, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out iXint))
                {
                    if (int.TryParse(iY, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out iYint))
                    {
                        int val2 = iXint.CompareTo(iYint);
                        if (this.direction)
                            return val2 * -1;
                        return val2;
                    }
                }

                int val = String.CompareOrdinal(iX, iY);
                if (this.direction)
                    return val * -1;
                return val;
            }

            public void swap()
            {
                this.direction = !this.direction;
            }
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ShowHelp();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowAbout();
        }

        private void addItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddItem();
        }

        private void tsbOpenPC_Click(object sender, EventArgs e)
        {
            Open(false);
        }

        private void tsbOpenConsole_Click(object sender, EventArgs e)
        {
            Open(true);
        }

        private void tsbSavePC_Click(object sender, EventArgs e)
        {
            Save(false);
        }

        private void saveAsPCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAs(false);
        }

        private void saveConsoleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save(true);
        }

        private void saveAsConsoleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAs(true);
        }

        private void tsbAddItem_Click(object sender, EventArgs e)
        {
            AddItem();
        }

        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Find();
        }

        private void debugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DebugDlg dlg = new DebugDlg(currentList);
            dlg.ShowDialog(this);
        }
    }
}
