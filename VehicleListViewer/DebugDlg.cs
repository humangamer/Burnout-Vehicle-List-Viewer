﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VehicleListViewer
{
    public partial class DebugDlg : Form
    {
        public DebugDlg()
        {
            InitializeComponent();
        }

        public DebugDlg(object obj) : this()
        {
            dbgMain.SelectedObject = obj;
        }
    }
}
