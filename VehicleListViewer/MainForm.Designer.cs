﻿namespace VehicleListViewer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openPCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.savePCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsPCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.findToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lstVehicles = new System.Windows.Forms.ListView();
            this.colIndex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colModel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBrand = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colWheelType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCategory = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colFlags = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBoost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMaxBoostlessSpeed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTopBoostSpeed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colNewUnknown = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colFinish = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colColor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colEngineID1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colEngineFile1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colGroupID1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colEngineID2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colEngineFile2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colGroupID2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.stsMain = new System.Windows.Forms.StatusStrip();
            this.stlStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbOpenPC = new System.Windows.Forms.ToolStripButton();
            this.tsbOpenConsole = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbSavePC = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbAddItem = new System.Windows.Forms.ToolStripButton();
            this.colDisplaySpeed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDisplayBoost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDisplayStrength = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mnuMain.SuspendLayout();
            this.stsMain.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.itemsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.debugToolStripMenuItem});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(1079, 24);
            this.mnuMain.TabIndex = 0;
            this.mnuMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openPCToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripMenuItem1,
            this.savePCToolStripMenuItem,
            this.saveAsPCToolStripMenuItem,
            this.toolStripMenuItem3,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openPCToolStripMenuItem
            // 
            this.openPCToolStripMenuItem.Image = global::VehicleListViewer.Properties.Resources.openHS;
            this.openPCToolStripMenuItem.Name = "openPCToolStripMenuItem";
            this.openPCToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openPCToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.openPCToolStripMenuItem.Text = "Open (PC)";
            this.openPCToolStripMenuItem.Click += new System.EventHandler(this.openPCToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = global::VehicleListViewer.Properties.Resources.openHS;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.openToolStripMenuItem.Text = "Open (Console)";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(229, 6);
            // 
            // savePCToolStripMenuItem
            // 
            this.savePCToolStripMenuItem.Image = global::VehicleListViewer.Properties.Resources.saveHS;
            this.savePCToolStripMenuItem.Name = "savePCToolStripMenuItem";
            this.savePCToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.savePCToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.savePCToolStripMenuItem.Text = "Save (PC)";
            this.savePCToolStripMenuItem.Click += new System.EventHandler(this.savePCToolStripMenuItem_Click);
            // 
            // saveAsPCToolStripMenuItem
            // 
            this.saveAsPCToolStripMenuItem.Image = global::VehicleListViewer.Properties.Resources.saveHS;
            this.saveAsPCToolStripMenuItem.Name = "saveAsPCToolStripMenuItem";
            this.saveAsPCToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsPCToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.saveAsPCToolStripMenuItem.Text = "Save As (PC)";
            this.saveAsPCToolStripMenuItem.Click += new System.EventHandler(this.saveAsPCToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(229, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // itemsToolStripMenuItem
            // 
            this.itemsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addItemToolStripMenuItem,
            this.toolStripMenuItem4,
            this.findToolStripMenuItem});
            this.itemsToolStripMenuItem.Name = "itemsToolStripMenuItem";
            this.itemsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.itemsToolStripMenuItem.Text = "Items";
            // 
            // addItemToolStripMenuItem
            // 
            this.addItemToolStripMenuItem.Image = global::VehicleListViewer.Properties.Resources.AddTableHS;
            this.addItemToolStripMenuItem.Name = "addItemToolStripMenuItem";
            this.addItemToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.addItemToolStripMenuItem.Text = "Add Item";
            this.addItemToolStripMenuItem.Click += new System.EventHandler(this.addItemToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(134, 6);
            // 
            // findToolStripMenuItem
            // 
            this.findToolStripMenuItem.Image = global::VehicleListViewer.Properties.Resources.FindHS;
            this.findToolStripMenuItem.Name = "findToolStripMenuItem";
            this.findToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.findToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.findToolStripMenuItem.Text = "Find";
            this.findToolStripMenuItem.Click += new System.EventHandler(this.findToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1,
            this.toolStripMenuItem2,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Image = global::VehicleListViewer.Properties.Resources.Help;
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(118, 22);
            this.helpToolStripMenuItem1.Text = "Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(115, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.debugToolStripMenuItem.Text = "Debug";
            this.debugToolStripMenuItem.Click += new System.EventHandler(this.debugToolStripMenuItem_Click);
            // 
            // lstVehicles
            // 
            this.lstVehicles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colIndex,
            this.colModel,
            this.colBrand,
            this.colName,
            this.colWheelType,
            this.colCategory,
            this.colFlags,
            this.colBoost,
            this.colMaxBoostlessSpeed,
            this.colTopBoostSpeed,
            this.colNewUnknown,
            this.colFinish,
            this.colColor,
            this.colDisplaySpeed,
            this.colDisplayBoost,
            this.colDisplayStrength,
            this.colEngineID1,
            this.colEngineFile1,
            this.colGroupID1,
            this.colEngineID2,
            this.colEngineFile2,
            this.colGroupID2});
            this.lstVehicles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstVehicles.FullRowSelect = true;
            this.lstVehicles.GridLines = true;
            this.lstVehicles.Location = new System.Drawing.Point(0, 49);
            this.lstVehicles.Name = "lstVehicles";
            this.lstVehicles.Size = new System.Drawing.Size(1079, 416);
            this.lstVehicles.TabIndex = 1;
            this.lstVehicles.UseCompatibleStateImageBehavior = false;
            this.lstVehicles.View = System.Windows.Forms.View.Details;
            this.lstVehicles.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstVehicles_ColumnClick);
            this.lstVehicles.SelectedIndexChanged += new System.EventHandler(this.lstVehicles_SelectedIndexChanged);
            this.lstVehicles.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstVehicles_MouseDoubleClick);
            // 
            // colIndex
            // 
            this.colIndex.Text = "Index";
            // 
            // colModel
            // 
            this.colModel.Text = "Model";
            this.colModel.Width = 80;
            // 
            // colBrand
            // 
            this.colBrand.Text = "Brand";
            this.colBrand.Width = 100;
            // 
            // colName
            // 
            this.colName.Text = "Name";
            this.colName.Width = 180;
            // 
            // colWheelType
            // 
            this.colWheelType.Text = "Wheel Type";
            this.colWheelType.Width = 160;
            // 
            // colCategory
            // 
            this.colCategory.Text = "Category";
            this.colCategory.Width = 80;
            // 
            // colFlags
            // 
            this.colFlags.Text = "Flags";
            this.colFlags.Width = 80;
            // 
            // colBoost
            // 
            this.colBoost.Text = "Boost";
            this.colBoost.Width = 90;
            // 
            // colMaxBoostlessSpeed
            // 
            this.colMaxBoostlessSpeed.Text = "Max Boostless Speed";
            this.colMaxBoostlessSpeed.Width = 80;
            // 
            // colTopBoostSpeed
            // 
            this.colTopBoostSpeed.Text = "Max Boost Speed";
            // 
            // colNewUnknown
            // 
            this.colNewUnknown.Text = "New Unknown";
            // 
            // colFinish
            // 
            this.colFinish.Text = "Finish";
            this.colFinish.Width = 80;
            // 
            // colColor
            // 
            this.colColor.Text = "Color";
            // 
            // colEngineID1
            // 
            this.colEngineID1.Text = "Engine ID 1";
            this.colEngineID1.Width = 120;
            // 
            // colEngineFile1
            // 
            this.colEngineFile1.Text = "Engine File 1";
            // 
            // colGroupID1
            // 
            this.colGroupID1.Text = "Group ID 1";
            this.colGroupID1.Width = 80;
            // 
            // colEngineID2
            // 
            this.colEngineID2.Text = "Engine ID 2";
            this.colEngineID2.Width = 120;
            // 
            // colEngineFile2
            // 
            this.colEngineFile2.Text = "Engine File 2";
            // 
            // colGroupID2
            // 
            this.colGroupID2.Text = "Group ID 2";
            this.colGroupID2.Width = 80;
            // 
            // stsMain
            // 
            this.stsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stlStatusLabel});
            this.stsMain.Location = new System.Drawing.Point(0, 465);
            this.stsMain.Name = "stsMain";
            this.stsMain.Size = new System.Drawing.Size(1079, 22);
            this.stsMain.TabIndex = 2;
            this.stsMain.Text = "statusStrip1";
            // 
            // stlStatusLabel
            // 
            this.stlStatusLabel.Name = "stlStatusLabel";
            this.stlStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbOpenPC,
            this.tsbOpenConsole,
            this.toolStripSeparator1,
            this.tsbSavePC,
            this.toolStripSeparator2,
            this.tsbAddItem});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1079, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbOpenPC
            // 
            this.tsbOpenPC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbOpenPC.Image = global::VehicleListViewer.Properties.Resources.openHS;
            this.tsbOpenPC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbOpenPC.Name = "tsbOpenPC";
            this.tsbOpenPC.Size = new System.Drawing.Size(23, 22);
            this.tsbOpenPC.Text = "Open (PC)";
            this.tsbOpenPC.Click += new System.EventHandler(this.tsbOpenPC_Click);
            // 
            // tsbOpenConsole
            // 
            this.tsbOpenConsole.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbOpenConsole.Image = global::VehicleListViewer.Properties.Resources.openHS;
            this.tsbOpenConsole.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbOpenConsole.Name = "tsbOpenConsole";
            this.tsbOpenConsole.Size = new System.Drawing.Size(23, 22);
            this.tsbOpenConsole.Text = "Open (Console)";
            this.tsbOpenConsole.Click += new System.EventHandler(this.tsbOpenConsole_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbSavePC
            // 
            this.tsbSavePC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSavePC.Image = global::VehicleListViewer.Properties.Resources.saveHS;
            this.tsbSavePC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSavePC.Name = "tsbSavePC";
            this.tsbSavePC.Size = new System.Drawing.Size(23, 22);
            this.tsbSavePC.Text = "Save (PC)";
            this.tsbSavePC.Click += new System.EventHandler(this.tsbSavePC_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbAddItem
            // 
            this.tsbAddItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAddItem.Image = global::VehicleListViewer.Properties.Resources.AddTableHS;
            this.tsbAddItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAddItem.Name = "tsbAddItem";
            this.tsbAddItem.Size = new System.Drawing.Size(23, 22);
            this.tsbAddItem.Text = "Add Item";
            this.tsbAddItem.Click += new System.EventHandler(this.tsbAddItem_Click);
            // 
            // colDisplaySpeed
            // 
            this.colDisplaySpeed.Text = "Display Speed";
            // 
            // colDisplayBoost
            // 
            this.colDisplayBoost.Text = "Display Boost";
            // 
            // colDisplayStrength
            // 
            this.colDisplayStrength.Text = "Display Strength";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1079, 487);
            this.Controls.Add(this.lstVehicles);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.stsMain);
            this.Controls.Add(this.mnuMain);
            this.MainMenuStrip = this.mnuMain;
            this.Name = "MainForm";
            this.Text = "Vehicle List Viewer";
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.stsMain.ResumeLayout(false);
            this.stsMain.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ListView lstVehicles;
        private System.Windows.Forms.ColumnHeader colIndex;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colBrand;
        private System.Windows.Forms.ColumnHeader colWheelType;
        private System.Windows.Forms.ToolStripMenuItem openPCToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader colCategory;
        private System.Windows.Forms.ColumnHeader colFlags;
        private System.Windows.Forms.ColumnHeader colGroupID1;
        private System.Windows.Forms.ColumnHeader colGroupID2;
        private System.Windows.Forms.StatusStrip stsMain;
        private System.Windows.Forms.ColumnHeader colMaxBoostlessSpeed;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem savePCToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel stlStatusLabel;
        private System.Windows.Forms.ColumnHeader colModel;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbOpenPC;
        private System.Windows.Forms.ToolStripButton tsbOpenConsole;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbSavePC;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbAddItem;
        private System.Windows.Forms.ToolStripMenuItem itemsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader colEngineID1;
        private System.Windows.Forms.ColumnHeader colEngineID2;
        private System.Windows.Forms.ColumnHeader colBoost;
        private System.Windows.Forms.ColumnHeader colFinish;
        private System.Windows.Forms.ToolStripMenuItem saveAsPCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ColumnHeader colTopBoostSpeed;
        private System.Windows.Forms.ColumnHeader colNewUnknown;
        private System.Windows.Forms.ColumnHeader colEngineFile1;
        private System.Windows.Forms.ColumnHeader colEngineFile2;
        private System.Windows.Forms.ColumnHeader colColor;
        private System.Windows.Forms.ColumnHeader colDisplaySpeed;
        private System.Windows.Forms.ColumnHeader colDisplayBoost;
        private System.Windows.Forms.ColumnHeader colDisplayStrength;
    }
}

